#include <iostream>
#include <vector>
#include <string>
#include <cstdlib>  //rand
#include <unistd.h> //sleep
#include <cstring>  //memset (will get rid of memset usage at some point)
#include<time.h>







#define RESET   "\033[0m"
#define BLACK   "\033[30m"      /* Black */
#define RED     "\033[31m"      /* Red */
#define GREEN   "\033[32m"      /* Green */
#define YELLOW  "\033[33m"      /* Yellow */
#define BLUE    "\033[34m"      /* Blue */
#define MAGENTA "\033[35m"      /* Magenta */
#define CYAN    "\033[36m"      /* Cyan */
#define WHITE   "\033[37m"      /* White */
#define BOLDBLACK   "\033[1m\033[30m"      /* Bold Black */
#define BOLDRED     "\033[1m\033[31m"      /* Bold Red */
#define BOLDGREEN   "\033[1m\033[32m"      /* Bold Green */
#define BOLDYELLOW  "\033[1m\033[33m"      /* Bold Yellow */
#define BOLDBLUE    "\033[1m\033[34m"      /* Bold Blue */
#define BOLDMAGENTA "\033[1m\033[35m"      /* Bold Magenta */
#define BOLDCYAN    "\033[1m\033[36m"      /* Bold Cyan */
#define BOLDWHITE   "\033[1m\033[37m"      /* Bold White */



using namespace std;



void makedisplayboard(unsigned int red,unsigned int white, unsigned int kings, string *dispboard)
{
	int i=0;
	unsigned int a = 0;

	for(i=0;i<32;++i)
	{
		a = 1<<i;
		//cout<<"mi a: "<<a<<endl;
		if ((red & a) == a)         			//if red piece (not king)
		{
			cout<<"rojo "<<(red & a)<<" "<<i<<endl;
			dispboard[i] = ((kings & a) == a)? "\033[1m\033[31m@\033[1m\033[33m":"\033[1m\033[31mo\033[1m\033[33m";
		}
		else if ((white & a) == a)				//if white piece (not king)
		{
			cout<<"blanco "<<(white & a)<<" "<<i<<endl;
			dispboard[i] = ((kings & a) == a ) ?"\033[1m\033[37m@\033[1m\033[33m":"\033[1m\033[37mo\033[1m\033[33m";
		}
		else 
		{
			//cout<<"espacio"<<" "<<i<<endl;
			dispboard[i]  = " ";
		}
		
		
	}
}

void printboard(vector<vector<string>> board)
{
	cout << BOLDRED<< "     0     1     2     3      " << endl;

	cout <<"  "<<BOLDYELLOW<<"-------------------------"<<endl;	

	cout << BOLDRED<<"a" <<BOLDYELLOW<< " " <<"|"<<"  "<<board[0][0]<< "  "<< "|"<<"  "<< board[0][1] << "  "<<"|"<<"  "<< board[0][2] << "  "<<"|"<<"  "<<board[0][3] <<"  "<<"|"<<endl;

	cout <<"  "<<BOLDYELLOW<<"|     |     |     |     |"<<endl;
	cout <<"  "<<BOLDYELLOW<<"|-----|-----|-----|-----|"<<endl;


	cout << BOLDRED<<"b" <<BOLDYELLOW<< " "<<"|"<<"  "<< board[1][0]  << "  "<<"|"<<"  "<< " " << "  "<<"|"<<"  "<<  board[1][2]   << "  "<<"|"<<"  "<< " " << "  "<<"|"<<"  "<< board[1][3]   << "  "<< endl;

	cout <<"  "<<BOLDYELLOW<<"|     |     |     |     |"<<endl;
	cout <<"  "<<BOLDYELLOW<<"|-----|-----|-----|-----|"<<endl;
	
	cout << BOLDRED<<"c" <<BOLDYELLOW<< " "<<"|"<<"  "<< board[2][0]  << "  "<<"|"<<"  "<< " " << "  "<<"|"<<"  "<<  board[2][2]   << "  "<<"|"<<"  "<< " " << "  "<<"|"<<"  "<< board[2][3]    << "  "<< endl;

	cout <<"  "<<BOLDYELLOW<<"|     |     |     |     |"<<endl;
	cout <<"  "<<BOLDYELLOW<<"|-----|-----|-----|-----|"<<endl;

	cout << BOLDRED<<"d" <<BOLDYELLOW<< " "<<"|"<<"  "<< board[3][0]  << "  "<<"|"<<"  "<< " " << "  "<<"|"<<"  "<<  board[3][2]   << "  "<<"|"<<"  "<< " " << "  "<<"|"<<"  "<< board[3][3]  << endl;

	cout <<"  "<<BOLDYELLOW<<"|     |     |     |     |"<<endl;

	cout <<"  "<<BOLDYELLOW<<"-------------------------"<<endl;	
	cout << RESET;
}







#include<vector>
int main()
{
		unsigned int red   = 983040; //hex representation of initial board for red pieces (F corresponds to a full row of four pieces) 0xF0000 0xFFF00000
		unsigned int white = 0xF; //initial board for white pieces
		unsigned int kings = 131072;	//no kings at the beginning
		vector<vector<string>> dispboard(5);
dispboard.reserve(32);
for(int i=0;i<4;i++) dispboard[i].reserve(4);
		dispboard[0]={" ","\033[1m\033[31mo\033[1m\033[33m"," ","\033[1m\033[31mo\033[1m\033[33m"};
		
		dispboard[1]={" "," "," "," "};
		dispboard[2]={" "," "," "," "};
			dispboard[3]={"\033[1m\033[37mo\033[1m\033[33m"," ","\033[1m\033[37mo\033[1m\033[33m"," "};
	srand (time(NULL));
		//makedisplayboard(red,white,kings,dispboard);
		printboard(dispboard);
	
	return 0;
}
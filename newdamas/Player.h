#ifndef PLAYER_H
#define PLAYER_H
#include"Fichas.h"
#include <bits/stdc++.h>
using namespace std;


class Player
{
 public:
  vector<Fichas> fichas;
  Player(){}
  Player(vector<Fichas> q) {
    fichas = q;
  }
  int getSize()
  {
    return fichas.size();
  }
  virtual ~Player() {}

 protected:

 private:
};

#endif // PLAYER_H

#include"MainBoard.h"
#include <bits/stdc++.h>



using namespace std;

/**
referencias

http://eric_rollins.home.mindspring.com/search/adversarialSearch.htm
http://www.dcc.fc.up.pt/~fds/aulas/PPD/1314/project3.html
https://aboorvadevarajan.github.io/Parallel-Checkers-Game/
http://disi.unitn.it/~montreso/asd/docs/checkers.pdf
https://es.wikipedia.org/wiki/Minimax


*/

int main()
{
  MainBoard juego(6);
  int opt;
  juego.printboard();//imprimo tablero
  while(1)
    {
      // juego.printboard();//imprimo tablero
      vector< Play > jugadas=juego.JugadasValidas(juego.human,dxh,dyh,POWN_HUMAN);//saco las jugadas validas para el jugador

      for(int i = 0; i < jugadas.size() ;++i)
      	cout<<i<<"| mover "<<jugadas[i].a.i<<" "<<jugadas[i].a.j<<" a: "<<jugadas[i].b.i<<" "<<jugadas[i].b.j<<endl;
      cout<<"\nescoja numero de jugada"<<endl;
      cin>>opt;
      juego.mutateBoard(juego,juego.human,jugadas,opt,0);
      cout << "------- Jugada Persona --------\n";
      // MainBoard JUG = juego.mini(juego,6,0,POWN_HUMAN);
      // juego = JUG;
      juego.printboard();
      MainBoard IA = juego.maxi(juego,6,0,POWN_COMP);
      juego = IA;
      cout << "------- Jugada Computadora --------\n";
      juego.printboard();
    }

  return 0;
}

#ifndef MAINBOARD_H
#define MAINBOARD_H
#include <bits/stdc++.h>
#include"Player.h"
#define POWN_COMP   "\033[1m\033[31mo\033[1m\033[33m"
#define POWN_HUMAN "\033[1m\033[37mo\033[30m\033[33m" //piea negra

#define ANCH_TABLERO 3
#define ALT_TABLERO 3

#define DEBUG
//#define POWN_HUMAN "\033[1m\033[37mo\033[1m\033[33m"    //piea blanca
#define loop(x,n) for(int x = 0; x < n; ++x)
int dxc[] =  {1,1};
int dyc[] = {1,-1};

int dxh[] = {-1,-1};
int dyh[] = {1,-1};


#define RESET   "\033[0m"
#define BLACK   "\033[30m"      /* Black */
#define RED     "\033[31m"      /* Red */
#define GREEN   "\033[32m"      /* Green */
#define YELLOW  "\033[33m"      /* Yellow */
#define BLUE    "\033[34m"      /* Blue */
#define MAGENTA "\033[35m"      /* Magenta */
#define CYAN    "\033[36m"      /* Cyan */
#define WHITE   "\033[37m"      /* White */
#define BOLDBLACK   "\033[1m\033[30m"      /* Bold Black */
#define BOLDRED     "\033[1m\033[31m"      /* Bold Red */
#define BOLDGREEN   "\033[1m\033[32m"      /* Bold Green */
#define BOLDYELLOW  "\033[1m\033[33m"      /* Bold Yellow */
#define BOLDBLUE    "\033[1m\033[34m"      /* Bold Blue */
#define BOLDMAGENTA "\033[1m\033[35m"      /* Bold Magenta */
#define BOLDCYAN    "\033[1m\033[36m"      /* Bold Cyan */
#define BOLDWHITE   "\033[1m\033[37m"      /* Bold White */


using namespace std;


struct coord
{
  int i;
  int j;
  coord(int x,int y)
  {
    i=x;
    j=y;
  }
  coord(){i =-1 ; j=-1;}
};

struct Play
{
  coord a,b,c;
  Play(coord na , coord nb , coord nc)
  {
    a = na ; b = nb ; c = nc;
  }
  Play(){}
};


class MainBoard
{
 public:

  int bsize,tokens; // Tama;o del tablero, numero de piezas
  Player human;//juegador humano con coordenadas de piezas
  Player comp;// computadora con coordenada de piezas
  vector< vector< string > > board; // Matriz de posiciones de fichas.

  void initTablero()
    {
      //espacios para cuadrar el tablero
      for(int i=0; i < bsize; ++i)
	{
	  for(int j = 0 ; j < bsize ; ++j)
	    {
	      board[i][j] = " ";
	    }
        }
    }

  void initTokens()
    {
      vector< Fichas > h,c; // generando piezas
      int lines = bsize * 3 / 4;
      lines%2 == 1 ? lines-- : 1;
      int inp=0,ini=1;
      /* c.push_back(Fichas(3,1)); */
      /* c.push_back(Fichas(3,3)); */
      /* h.push_back(Fichas(1,1)); */
      for(int i = 0 ; i < lines/2 ; ++i)
      	{
      	  if(i % 2 == 0)
      	    for(int j = inp ; j < bsize; j+=2)
      	      {
      		Fichas fich(i,j);
      		h.push_back(fich);
      	      }
      	  else
      	    for(int j = ini ; j < bsize; j+=2)
      	      {
      		Fichas fich(i,j);
      		h.push_back(fich);
      	      }
	    
      	}

      for(int i = bsize - 1 ; i >= bsize - lines/2 ; --i)
      	{
      	  if(i % 2 == 0)
      	    for(int j = inp ; j < bsize; j+=2)
      	      {
      		Fichas fich(i,j);
      		c.push_back(fich);
      	      }
      	  else
      	    for(int j = ini ; j < bsize; j+=2)
      	      {
      		Fichas fich(i,j);
      		c.push_back(fich);
      	      }
	    
      	}
      
      human = Player(c);
      comp = Player(h);
    }
  void refreshTokens(Player Gamer, bool type)
  {
    for(int i = 0; i < Gamer.getSize(); i++) // Itero sobre todas las piezas de la persona
      {
	if(type == 0)
	  board[human.fichas[i].pos_i][human.fichas[i].pos_j]=POWN_HUMAN;
	else
	  board[comp.fichas[i].pos_i][comp.fichas[i].pos_j]=POWN_COMP;
      }
  }
  void eraseCell(Fichas f)
  {
    board[f.pos_i][f.pos_j] = " ";
  }
  
  void refreshAll()
    {
      refreshTokens(comp,1);
      refreshTokens(human,0);
    }

  MainBoard(){}
  MainBoard(Player a , Player b)
    {
      comp = a;
      human = b;
    }
  MainBoard(int n)
    {
      bsize= n;
      board.resize(bsize * bsize);//reservo n*n casilleros
      for(unsigned int  i=0; i<board.size(); i++) //reservo espacios para cada fila
	board[i].resize(n);
      initTablero();
      initTokens();
      refreshAll();
      //inicializo las coordenadas donde se hubicaran las piezas
    }
  /*
    evaluo que la posicion no este ocupada por alguna ficha
  */

  bool vacio(int i,int j)
  {
    return board[i][j]!= POWN_HUMAN && board[i][j]!= POWN_COMP;
  }

  bool esValido(Fichas f, string tipo)
  {
    /* refreshAll(); */
    if(f.pos_i <= bsize - 1 and f.pos_i >= 0 and f.pos_j <= bsize -1 and f.pos_j >= 0 and board[f.pos_i][f.pos_j] != tipo)
      return 1;
    return 0;
  }

  /* bool esValidoComer(Fichas f, string tipo) */
  /* { */
  /*   if(board[f.pos.i][f.pos]) */
  /* } */

  bool eatToken(Fichas f, string tipo)
  {
    if(board[f.pos_i][f.pos_j] != " ") // como
      return 1;
    return 0;
  }
  
  void yummyToken(Fichas f, bool type)
  {
    if(f.pos_i != -1 and f.pos_j != -1)
      {
	if(type == 0)
	  {
	    for(int i = 0 ; i < comp.getSize() ; ++i)
	      {
		if(f.pos_i == comp.fichas[i].pos_i and f.pos_j == comp.fichas[i].pos_j)
		  {
		    comp.fichas.erase(comp.fichas.begin()+i);
		    break;
		  }
	      }
	  }
	if(type == 1)
	  {
	    for(int i = 0 ; i < human.getSize(); ++i)
	      {
		if(f.pos_i == human.fichas[i].pos_i and f.pos_j == human.fichas[i].pos_j)
		  {
		    human.fichas.erase(human.fichas.begin()+i);
		    
		  }
	      }
	  }
	eraseCell(f);
      }
  }
  void mutateBoard(MainBoard &b, Player &p, vector<Play> t, int opt,bool tip)
  {
    for(int i =0 ; i < p.getSize() ; ++i)
      {
	if(p.fichas[i].pos_i == t[opt].a.i && p.fichas[i].pos_j==t[opt].a.j)
	  {
	    b.eraseCell(p.fichas[i]);
	    Fichas ff(t[opt].c.i,t[opt].c.j);
	    b.yummyToken(ff,tip);
	    p.fichas[i].pos_i=t[opt].b.i;
	    p.fichas[i].pos_j=t[opt].b.j;
	    break;
	  }

      }
    b.refreshAll();
  }
  
  //type: 0 para jugadas del ordenador
  //      1 para jugadas del jugador
  vector< Play> JugadasValidas(Player &gamer,int dx[], int dy[], string tipo)
    { 
      vector< Play> jugadas; //la primera coordenada representa a la ficha, la segunda representa a donde lo queremos mover
      /* cout << gamer.getSize()<< "\n"; */
	  for(int i=0; i< gamer.getSize(); i++)
	    {
	      Fichas ficha = gamer.fichas[i];
	      for(int i = 0 ; i < 2 ; ++i)
		{
		  Fichas nw = Fichas(ficha.pos_i + dx[i],ficha.pos_j + dy[i]);
		  Fichas eating = Fichas(-1,-1);
		  if(esValido(nw,tipo))
		    {
		      if(eatToken(nw,tipo))
			{
			  eating = nw;
			  Fichas f(nw.pos_i + dx[i],nw.pos_j + dy[i]);
			  if( esValido(f,tipo) )
			    {
			      nw.pos_i += dx[i];
			      nw.pos_j += dy[i];
			      if(!eatToken(nw,tipo))
				{
				  Play state(coord(ficha.pos_i,ficha.pos_j),coord(nw.pos_i,nw.pos_j), coord(eating.pos_i,eating.pos_j)) ;
				  jugadas.push_back(state);
				}
			    }
			}
		      else 
			{
			  Play state(coord(ficha.pos_i,ficha.pos_j),coord(nw.pos_i,nw.pos_j), coord(eating.pos_i,eating.pos_j)) ;
			  jugadas.push_back(state);
			}
		    }
		}
            }

      return jugadas;
    }
  
  int dif_gamers(Player a, Player b)
  {
    return a.getSize()-b.getSize();
  }
    /*
        table de juego y n = nivel
    */
    MainBoard maxi(MainBoard tabla,int n,int ini,string tipo)
    {
      /* cout << "MAXI\n"; */
      if(ini==n) return tabla;
      Player current = tabla.comp;
      vector< Play > jugadas = tabla.JugadasValidas(current,dxc,dyc,POWN_COMP);//saco las jugadas validas para el jugador
      vector<MainBoard> estados;//vector de posibles jugadas
      for(int i = 0 ; i < jugadas.size() ; ++i)
        {
      	  MainBoard temp = tabla;
      	  temp.mutateBoard(temp,temp.comp,jugadas,i,1);
      	  estados.push_back(temp);
      	}
      int t = -100;
      MainBoard rpta ;
      for(int i = 0 ; i < estados.size() ; ++i)
      	{
	  MainBoard aux = mini(estados[i],n,ini+1,POWN_HUMAN);
	  if(aux.dif_gamers(aux.comp,aux.human) > t )
	    {
	      t = aux.dif_gamers(aux.comp,aux.human);
	      rpta = estados[i];
	    }
	  /* estados[i].printboard(); */
      	}
      return rpta;
    }

    MainBoard mini(MainBoard tabla,int n,int ini,string tipo)
    {
      /* cout << "MINI\n"; */
      if(ini==n) return tabla;
      Player current = tabla.human;
      vector< Play > jugadas = tabla.JugadasValidas(current,dxh,dyh,POWN_HUMAN);//saco las jugadas validas para el jugador
      vector<MainBoard> estados;//vector de posibles jugadas
      for(int i = 0 ; i < jugadas.size() ; ++i)
        {
      	  MainBoard temp = tabla;
      	  temp.mutateBoard(temp,temp.human,jugadas,i,0);
      	  estados.push_back(temp);
      	}
      int t = 1000000;
      MainBoard rpta ;
      for(int i = 0 ; i < estados.size() ; ++i)
      	{
	  MainBoard aux = maxi(estados[i],n,ini+1,POWN_COMP);
	  if(aux.dif_gamers(aux.human,aux.comp) < t )
	    {
	      t = aux.dif_gamers(aux.human,aux.comp);
	      rpta = estados[i];
	    }
	  /* estados[i].printboard(); */
      	}
      return rpta;
    }
      

    void printboard()
    {
      for(int i = 0 ; i < bsize ; ++i )
	printf(BOLDRED "%6d",i);
      printf("\n");
      printf("   ");
      for(int i = 0 ; i < bsize ; ++i)
	printf(BOLDYELLOW "------");
      printf("\n");

      for(int i = 0 ; i < bsize ; ++i)
	{
	  printf(BOLDRED "%d",i);
	  printf(BOLDYELLOW " |");
	  for(int j =0 ; j < bsize ; ++j)
	    {
	      cout << "  " << board[i][j] << "  |";
	    }
	  printf("\n");
	  printf(BOLDYELLOW "  |");
	  for(int j = 0 ; j < bsize ; ++j)
	    {
	      printf("     |");
	    }
	  printf("\n");
	  printf(BOLDYELLOW "  |");
	  for(int j = 0 ; j < bsize ; ++j)
	    printf("-----|");
	  printf("\n");
	}
      printf(RESET);
    }
  virtual ~MainBoard() {}

};

#endif // MAINBOARD_H

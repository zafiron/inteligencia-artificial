#include <iostream>

#include"Graphs.h"
using namespace std;


/*
    objetivo : crear un algoritmo de busqueda a ciegas y otro con heuristica
*/



int main()
{
    Graph< string , int > Alfa;
    Alfa.AddNode( "Arequipa" , 9 );
    Alfa.AddNode( "Tacna", 10 );
    Alfa.AddNode( "Moquegua", 4 );
    Alfa.AddNode( "Ica",  3 );
    Alfa.AddNode( "Ucayali", 5 );
    Alfa.AddNode( "La Libertad", 6 );
    Alfa.AddNode( "Cajamarca", 2 );
    Alfa.AddNode( "Loreto", 9);
    Alfa.AddNode( "Mollendo", 7 );
    Alfa.AddNode( "Tumbes", 8 );
    Alfa.AddNode("Lima", 1 );
    Alfa.AddRelationShipBetween("Arequipa","Tacna");
    Alfa.AddRelationShipBetween("Arequipa","Moquegua");
    Alfa.AddRelationShipBetween("Moquegua","Mollendo");
    Alfa.AddRelationShipBetween("Moquegua","La Libertad");
    Alfa.AddRelationShipBetween("La Libertad","Mollendo");
    Alfa.AddRelationShipBetween("Ica","Loreto");
    Alfa.AddRelationShipBetween("Ica","Cajamarca");
    Alfa.AddRelationShipBetween("Ica","Tumbes");
    Alfa.AddRelationShipBetween("Tacna","Ica");
    Alfa.AddRelationShipBetween("Ucayali","Ica");
    Alfa.AddRelationShipBetween("Mollendo","Ucayali");
    Alfa.AddRelationShipBetween( "Ucayali","Lima");
    cout << Alfa.BreadthFirstSearch( "Ica","Lima");
    cout << "Hello World!" << endl;
    return 0;
}


#ifndef GRAPHS_H
#define GRAPHS_H

#include<iostream>
#include<map>
#include<list>
#include<utility>
#include<set>
#include<queue>

using namespace std;

template< class T, class Tr >
class Graph{
public:
    /*
     *  StorageInside = list of neighborns
     *  NodeData = pair with first => value of Node and second=> StorageInside
     *  Storage = graph with all node( NodeData )
    */
    typedef list< T > StorageInside;
    typedef pair< Tr , StorageInside* > NodeData ;
    typedef map< T , NodeData  > Storage;

    Graph();
    virtual ~Graph();

    // function to create a graph

    bool AddNode( T a, Tr b );
    void ModifyWeight( T a , Tr b );
    bool AddRelationShipBetween( T a , T  b);
    bool EraseNode( T a );
    bool EraseRelationShipBetween( T a , T b );

    // function to Search

    bool BreadthFirstSearch( T a , T b );


private:
    Storage* Warehouse;
};

////////////// constructor and destructor

template< class T, class Tr >
Graph< T , Tr >::Graph(){
    Warehouse = 0;
}

template< class T , class Tr >
Graph< T , Tr >::~Graph(){
    if( Warehouse == 0 ){
        delete Warehouse;
    }
    Warehouse = 0;
}

////////// body of class

template< class T, class Tr >
bool Graph< T, Tr >::AddNode( T a , Tr b ){
    if( Warehouse == 0 ){
        Warehouse = new Storage;
    }

    typename Storage::iterator itr = Warehouse->find( a );
    typename Storage::iterator itrEnd = Warehouse->end();
    if( itr !=  itrEnd ){
        return false;
    }

    Warehouse->insert( pair< T, NodeData>( a , pair< Tr , StorageInside*  >( b , 0 ) ));
    return true;

}

template< class T , class Tr >
bool Graph< T , Tr >::AddRelationShipBetween(T a, T b){
    if( Warehouse == 0 ){
        return false;
    }

    typename Storage::iterator itr1 = Warehouse->find( a );
    typename Storage::iterator itr2 = Warehouse->find( b );
    typename Storage::iterator itrEnd = Warehouse->end();

    if( itr1  == itrEnd || itr2 == itrEnd ){
        return false;
    }

    if( (*itr1).second.second == 0 ){
        (*itr1).second.second = new StorageInside;
    }
    if( (*itr2).second.second == 0 ){
        (*itr2).second.second = new StorageInside;
    }

    (*itr2).second.second->push_back( a );
    (*itr1).second.second->push_back( b );
    return true;
}

/////////// function to search

template< class T , class Tr >
bool Graph< T, Tr >::BreadthFirstSearch( T a, T b ){
    bool Rpta = false;
    int Level = 0;
    set< T >Visited;
    queue< T > InList;
    InList.push( a );
    Visited.insert( a );

    while( !InList.empty() ){
        Level++;
        cout << "Level " << Level << ": ";
        for( int Limit = InList.size() ; Limit > 0 ; Limit-- ){
            Visited.insert( InList.front() );
            cout << InList.front() << " ";
            StorageInside* ptr = ( *( this->Warehouse->find( InList.front() ) ) ).second.second;
            InList.pop();

            for( typename StorageInside::iterator itrIns = ptr->begin(); itrIns != ptr->end() ; itrIns++ ){
                ///cout << *itrIns << " ";
                if( (*itrIns ) == b  ){
                    cout << endl << "Congratulations you arrived to "<< b << ".The long of path is " << Level << endl;
                    return Rpta =true;
                }else if( Visited.find( ( *itrIns ) ) == Visited.end() ){
                    InList.push( ( *itrIns ) );
                }
            }

        }
        cout << endl;
    }
    cout << " Sorry, you never arrive!!!" << endl;
    return Rpta;
}


#endif // GRAPHS_H

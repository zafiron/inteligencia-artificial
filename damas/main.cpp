#include <iostream>
#include"MainBoard.h"




using namespace std;

/**
referencias

http://eric_rollins.home.mindspring.com/search/adversarialSearch.htm
http://www.dcc.fc.up.pt/~fds/aulas/PPD/1314/project3.html
https://aboorvadevarajan.github.io/Parallel-Checkers-Game/
http://disi.unitn.it/~montreso/asd/docs/checkers.pdf
https://es.wikipedia.org/wiki/Minimax


*/


int main()
{
    Player p1(3,0,3,2),p2(0,1,0,3);
    MainBoard juego(p1,p2);
    

    int opt;
    while(1)
    {
        juego.printboard();//imprimo tablero
        vector<pair<coord,coord> > jugadas=juego.JugadasValidas(juego.human,1);//saco las jugadas validas para el jugador

        loop(i,jugadas.size())
            cout<<i<<"| mover "<<jugadas[i].first.i<<" "<<jugadas[i].first.j<<" a: "<<jugadas[i].second.i<<" "<<jugadas[i].second.j<<endl;

        cout<<"\nescoja numero de jugada"<<endl;
        cin>>opt;

       // cout<<jugadas[opt].second.i<<" "<<jugadas[opt].second.j<<endl;
        loop(i,jugadas.size())
        {
            if(juego.human.fichas[i].pos_i==jugadas[i].first.i && juego.human.fichas[i].pos_j==jugadas[i].first.j && i==opt)
            {
                            juego.human.fichas[i].pos_i=jugadas[i].second.i;
            juego.human.fichas[i].pos_j=jugadas[i].second.j;
            }


        }
       juego.board[jugadas[opt].first.i][jugadas[opt].first.j]=" ";
       juego.board[jugadas[opt].second.i][jugadas[opt].second.j]=POWN_HUMAN;
       MainBoard rpt=juego.mini(juego,2);
        juego.comp=rpt.comp;
        juego.inicializa_fichas();
       juego.printboard();
    }


    return 0;
}

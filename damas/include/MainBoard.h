#ifndef MAINBOARD_H
#define MAINBOARD_H

#include<iostream>
#include<vector>
#include<string>
#include"Player.h"
#define POWN_COMP   "\033[1m\033[31mo\033[1m\033[33m"


#define ANCH_TABLERO 3
#define ALT_TABLERO 3

#define DEBU
//#define POWN_HUMAN "\033[1m\033[37mo\033[1m\033[33m"    //piea blanca
#define POWN_HUMAN "\033[1m\033[30mo\033[30m\033[33m" //piea negra
#define loop(x,n) for(int x = 0; x < n; ++x)



#define RESET   "\033[0m"
#define BLACK   "\033[30m"      /* Black */
#define RED     "\033[31m"      /* Red */
#define GREEN   "\033[32m"      /* Green */
#define YELLOW  "\033[33m"      /* Yellow */
#define BLUE    "\033[34m"      /* Blue */
#define MAGENTA "\033[35m"      /* Magenta */
#define CYAN    "\033[36m"      /* Cyan */
#define WHITE   "\033[37m"      /* White */
#define BOLDBLACK   "\033[1m\033[30m"      /* Bold Black */
#define BOLDRED     "\033[1m\033[31m"      /* Bold Red */
#define BOLDGREEN   "\033[1m\033[32m"      /* Bold Green */
#define BOLDYELLOW  "\033[1m\033[33m"      /* Bold Yellow */
#define BOLDBLUE    "\033[1m\033[34m"      /* Bold Blue */
#define BOLDMAGENTA "\033[1m\033[35m"      /* Bold Magenta */
#define BOLDCYAN    "\033[1m\033[36m"      /* Bold Cyan */
#define BOLDWHITE   "\033[1m\033[37m"      /* Bold White */


using namespace std;

struct coord
{
    int i;
    int j;
    coord(int x,int y)
    {
        i=x;
        j=y;
    }
};
class MainBoard
{
public:
    Player human{3,0,3,2};//juegador humano con coordenadas de piezas
    Player comp{0,2,0,3};// computadora con coordenada de piezas
    vector<vector<string> > board{4}; //array donde se ubicaran las piezas 4 filas


    MainBoard(Player h,Player c)
    {
        human=h;
        comp=c;

        //cout<<board.size()<<endl;
        board.reserve(16);//reservo 16 casilleros
        for(unsigned int  i=0; i<board.size(); i++) //reservo espacios para cada fila
            board[i].resize(4);
        //board[0].resize(4);

        //espacios para cuadrar el tablero

        cout<<"constructor human"<<endl;
        cout<<human.fichas[0].pos_i<<" "<<human.fichas[0].pos_j<<endl;
        cout<<human.fichas[1].pos_i<<" "<<human.fichas[1].pos_j<<endl;

        cout<<"constructor comp"<<endl;
        cout<<comp.fichas[0].pos_i<<" "<<comp.fichas[0].pos_j<<endl;
        cout<<comp.fichas[1].pos_i<<" "<<comp.fichas[1].pos_j<<endl;
        //inicializo las coordenadas donde se hubicaran las piezas
     inicializa_fichas();

    }
    void inicializa_fichas()
    {

        for(int i=0; i<4; i++)
        {
            board[0][i]=" ";
            board[1][i]=" ";
            board[2][i]=" ";
            board[3][i]=" ";
        }
   for(int i=0; i<2; i++) //solo itero hasta 2 por que solo son 2 piezas
        {
            board[human.fichas[i].pos_i][human.fichas[i].pos_j]=POWN_HUMAN;//coordenadas piezas humanas
            board[comp.fichas[i].pos_i][comp.fichas[i].pos_j]=POWN_COMP; // coordenadas piezas comp
            comp.fichas[i].is_king=true;
        }
    }

    /*
        evaluo que la posicion no este ocupada por alguna ficha
    */
    bool vacio(int i,int j)
    {
        return board[i][j]!= POWN_HUMAN && board[i][j]!= POWN_COMP;
    }
    //type: 0 para jugadas del ordenador
    //      1 para jugadas del jugador
    vector<pair<coord,coord> > JugadasValidas(Player &gamer,bool type)
    {
        vector<pair<coord,coord > > jugadas; //la primera coordenada representa a la ficha, la segunda representa a donde lo queremos mover
        if(type==0)
        {

            for(int i=0; i<2; i++)
            {
                Fichas ficha = gamer.fichas[i];
#ifdef DEBUG
                cout<<"ficha actual "<<ficha.pos_i<<" "<<ficha.pos_j<<endl;
#endif // DEBUG
                if(ficha.pos_i+1<=ALT_TABLERO)//evaluo si la fila+1 es menor que el limite del tablero
                {
                    if(ficha.pos_j+1<=ANCH_TABLERO && vacio(ficha.pos_i+1,ficha.pos_j+1)  )//EVAluo si la col+1 es menor que el limite del tablero
                    {
                        pair<coord,coord> g(coord(ficha.pos_i,ficha.pos_j),coord(ficha.pos_i+1,ficha.pos_j+1));
                        jugadas.push_back(g);
                    }

                    if(ficha.pos_j-1>=0 && vacio(ficha.pos_i+1,ficha.pos_j-1) )
                    {
                        pair<coord,coord> g(coord(ficha.pos_i,ficha.pos_j),coord(ficha.pos_i+1,ficha.pos_j-1));
                        jugadas.push_back(g);

                    }
                }
                if(ficha.is_king)//es rey entonces añado jugadas validas para el
                {
                    if(ficha.pos_i-1>=0)
                    {


                        if(ficha.pos_j+1<=ANCH_TABLERO && vacio(ficha.pos_i-1,ficha.pos_j+1))//EVAluo si la col+1 es menor que el limite del tablero
                        {
                            pair<coord,coord> g(coord(ficha.pos_i,ficha.pos_j),coord(ficha.pos_i-1,ficha.pos_j+1));
                            jugadas.push_back(g);
                        }

                        if(ficha.pos_j-1>=0 && vacio(ficha.pos_i-1,ficha.pos_j-1))
                        {
                            pair<coord,coord> g(coord(ficha.pos_i,ficha.pos_j),coord(ficha.pos_i-1,ficha.pos_j-1));
                            jugadas.push_back(g);

                        }
                    }
                }



            }
        }
        else
        {
            for(int i=0; i<2; i++)
            {
                Fichas ficha = gamer.fichas[i];
#ifdef DEBUG
                cout<<"ficha actual "<<ficha.pos_i<<" "<<ficha.pos_j<<endl;
#endif // DEBUG
                if(ficha.pos_i-1<=ALT_TABLERO)//evaluo si la fila+1 es menor que el limite del tablero
                {
                    if(ficha.pos_j+1<=ANCH_TABLERO && vacio(ficha.pos_i-1,ficha.pos_j+1)  )//EVAluo si la col+1 es menor que el limite del tablero
                    {
                        pair<coord,coord> g(coord(ficha.pos_i,ficha.pos_j),coord(ficha.pos_i-1,ficha.pos_j+1));
                        jugadas.push_back(g);
                    }

                    if(ficha.pos_j-1>=0 && vacio(ficha.pos_i-1,ficha.pos_j-1) )
                    {
                        pair<coord,coord> g(coord(ficha.pos_i,ficha.pos_j),coord(ficha.pos_i-1,ficha.pos_j-1));
                        jugadas.push_back(g);

                    }
                }
                if(ficha.is_king)//es rey entonces añado jugadas validas para el
                {
                    if(ficha.pos_i+1>=0)
                    {

                        if(ficha.pos_j+1<=ANCH_TABLERO && vacio(ficha.pos_i+1,ficha.pos_j+1))//EVAluo si la col+1 es menor que el limite del tablero
                        {
                            pair<coord,coord> g(coord(ficha.pos_i,ficha.pos_j),coord(ficha.pos_i+1,ficha.pos_j+1));
                            jugadas.push_back(g);
                        }

                        if(ficha.pos_j-1>=0 && vacio(ficha.pos_i+1,ficha.pos_j-1))
                        {
                            pair<coord,coord> g(coord(ficha.pos_i,ficha.pos_j),coord(ficha.pos_i+1,ficha.pos_j-1));
                            jugadas.push_back(g);

                        }
                    }
                }
            }

        }

        return jugadas;
    }

    int dif_gamers()
    {
        return comp.fichas.size()-human.fichas.size();
    }
    /*
        table de juego y n = nivel
    */
    MainBoard maxi(MainBoard tabla,int n,int ini=0)
    {
        cout<<"==MI INI MAX==  "<<ini<<endl;
        if(ini==n) return tabla;

        Player current = tabla.comp;
        vector<pair<coord,coord> > jugadas=tabla.JugadasValidas(current,0);//saco las jugadas validas para el jugador
        vector<MainBoard> estados;///vector de posibles jugadas
        loop(ii,jugadas.size())//itero las jugadas validas del ordenador
        {
            //cout<<ii<<"| mover "<<jugadas[ii].first.i<<" "<<jugadas[ii].first.j<<" a: "<<jugadas[ii].second.i<<" "<<jugadas[ii].second.j<<endl;


            //for(int i=0; i<2; i++)
            // {
            if(current.fichas[0].pos_i==jugadas[ii].first.i && current.fichas[0].pos_j==jugadas[ii].first.j )//si las coordenadas de mi ficha son igual a la de la jugada
            {
                Player c_temp(current.fichas[1].pos_i,current.fichas[1].pos_j,jugadas[ii].second.i,jugadas[ii].second.j);

                MainBoard temp(tabla.human,c_temp);
                #ifdef DEBUG
                temp.printboard();
                #endif // DEBUG
                estados.push_back(temp);
            }
            else if(current.fichas[1].pos_i==jugadas[ii].first.i && current.fichas[1].pos_j==jugadas[ii].first.j )
            {
                Player c_temp(current.fichas[0].pos_i,current.fichas[0].pos_j,jugadas[ii].second.i,jugadas[ii].second.j);

                MainBoard temp(tabla.human,c_temp);
                #ifdef DEBUG
                temp.printboard();
                #endif // DEBUG
                estados.push_back(temp);
            }
            //}
        }


        //itero los estados
        MainBoard temp(Player(0,0,0,0),Player(0,0,0,0));
        int t=-100;
        int deep = ++ini;
        loop(ii,estados.size())
        {
            MainBoard r = mini(estados[ii],n,deep);
            if(r.dif_gamers()>t)//asigno el mayor
            {
                temp=r;
                 //temp.printboard();
            }
            //estados[ii].printboard();
        }
        return temp;
    }

    MainBoard mini(MainBoard tabla,int n,int ini=0)
    {
        cout<<"==MI INI MIN==  "<<ini<<endl;
        if(ini==n) return tabla;

        Player current = tabla.human;
        vector<pair<coord,coord> > jugadas=tabla.JugadasValidas(current,1);//saco las jugadas validas para el jugador
        vector<MainBoard> estados;///vector de posibles jugadas
        loop(ii,jugadas.size())//itero las jugadas validas del ordenador
        {
            cout<<ii<<"| mover "<<jugadas[ii].first.i<<" "<<jugadas[ii].first.j<<" a: "<<jugadas[ii].second.i<<" "<<jugadas[ii].second.j<<endl;


            //for(int i=0; i<2; i++)
            // {
            if(current.fichas[0].pos_i==jugadas[ii].first.i && current.fichas[0].pos_j==jugadas[ii].first.j )//si las coordenadas de mi ficha son igual a la de la jugada
            {
                Player c_temp(current.fichas[1].pos_i,current.fichas[1].pos_j,jugadas[ii].second.i,jugadas[ii].second.j);

                MainBoard temp(c_temp,tabla.comp);
                 #ifdef DEBUG
                temp.printboard();
                #endif // DEBUG
                estados.push_back(temp);
            }
            else if(current.fichas[1].pos_i==jugadas[ii].first.i && current.fichas[1].pos_j==jugadas[ii].first.j )
            {
                Player c_temp(current.fichas[0].pos_i,current.fichas[0].pos_j,jugadas[ii].second.i,jugadas[ii].second.j);

                MainBoard temp(c_temp,tabla.comp);
                 #ifdef DEBUG
                temp.printboard();
                #endif // DEBUG
                estados.push_back(temp);
            }
            //}
        }


        //itero los estados
        MainBoard temp(Player(0,0,0,0),Player(0,0,0,0));
        int t=100;
        int deep  =++ini;
        loop(ii,estados.size())
        {
            MainBoard r = maxi(estados[ii],n,deep);
            if(r.dif_gamers()<t)//asigno el mayor
            {
                temp=r;
               // temp.printboard();
            }
            //estados[ii].printboard();
        }
        return temp;
    }

    /*

    */
    void printboard()
    {
        cout << BOLDRED<< "     0     1     2     3      " << endl;
        cout <<"  "<<BOLDYELLOW<<"-------------------------"<<endl;

        cout << BOLDRED<<"0" <<BOLDYELLOW<< " " <<"|"<<"   "<<board[0][0]<< " "<< "|"<<"  "<< board[0][1] << "  "<<"|"<<"  "<< board[0][2] << "  "<<"|"<<"  "<<board[0][3] <<"  "<<"|"<<endl;

        cout <<"  "<<BOLDYELLOW<<"|     |     |     |     |"<<endl;
        cout <<"  "<<BOLDYELLOW<<"|-----|-----|-----|-----|"<<endl;


        cout << BOLDRED<<"1" <<BOLDYELLOW<< " "<<"|"<<"   "<< board[1][0] << " "<< "|"<<"  "<< board[1][1] << "  "<<"|"<<"  "<<  board[1][2]   << "  "<<"|"<<"  "<< board[1][3]  << "  "<<"|"<< endl;

        cout <<"  "<<BOLDYELLOW<<"|     |     |     |     |"<<endl;
        cout <<"  "<<BOLDYELLOW<<"|-----|-----|-----|-----|"<<endl;

        cout << BOLDRED<<"2" <<BOLDYELLOW<< " "<<"|"<<"   "<< board[2][0] << " "<< "|"<<"  "<< board[2][1] << "  "<<"|"<<"  "<< board[2][2]   << "  "<<"|"<<"  "<< board[2][3]  << "  "<<"|" << endl;

        cout <<"  "<<BOLDYELLOW<<"|     |     |     |     |"<<endl;
        cout <<"  "<<BOLDYELLOW<<"|-----|-----|-----|-----|"<<endl;

        cout << BOLDRED<<"3" <<BOLDYELLOW<< " "<<"|"<<"   "<< board[3][0] << " "<< "|"<<"  "<< board[3][1] << "  "<<"|"<<"  "<<  board[3][2]   << "  "<<"|"<<"  "<< board[3][3] << "  "<<"|"<< endl;

        cout <<"  "<<BOLDYELLOW<<"|     |     |     |     |"<<endl;
        cout <<"  "<<BOLDYELLOW<<"-------------------------"<<endl;
        cout << RESET;
    }
    void jugar()
    {

    }
    virtual ~MainBoard() {}

};

#endif // MAINBOARD_H

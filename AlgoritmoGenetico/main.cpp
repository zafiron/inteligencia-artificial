#include <iostream>

#include "GeneticAlgorithm.h"
#define MAXF 70
#define MAXC 10
using namespace std;

class Funcion{
public:
    Funcion(){}
    ~Funcion(){}
    int operator()( int a ){
       return a*a - 4*a + 5;
    }
};

class Traits{
public:
    typedef Funcion F;
};
char Matrix[MAXF][MAXC];
int main()
{
    Funcion A;
    GeneticAlgorithm< Traits > rpta;
    rpta.GetFunction( A );
    rpta.Iteration( 10 , 0 , 11 );
    vector<double> ans = rpta.ans;
    cout << ans.size();
    for(int  i = 0 ; i < MAXF ; ++i)
        for(int  j = 0 ; j < MAXC ; ++j)
            Matrix[i][j]=' ';
    for(int i = 0 ; i < ans.size() ; ++i)
        Matrix[MAXF - int(ans[i])][i] = '*';
    for(int  i = 0 ; i < MAXF ; ++i)
    {
        for(int  j = 0 ; j < MAXC ; ++j)
            cout << Matrix[i][j] << " ";
        cout << endl;
    }
    return 0;
}

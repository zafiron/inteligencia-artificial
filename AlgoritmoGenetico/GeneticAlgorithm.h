#ifndef GENETICALGORITHM_H
#define GENETICALGORITHM_H

#include<vector>
#include<cstdlib>
#include<ctime>
#include<cmath>
#include<utility>
#include<map>
#include<algorithm>
#include<iostream>
#include<unistd.h>

using namespace std;


/*
 * Algoritmo genetico para encontrar el maximo valor de un funcion cuadratica en un rango dado
*/

/*  R = valore que recibe F
 *   F = Funcion a evaluar
*/

template< class Tr >
class GeneticAlgorithm{

    typedef int R;
    typedef typename Tr::F F;
    typedef R Population_type;
    typedef vector< Population_type > Population;

public:
    GeneticAlgorithm( );
    void GetFunction( F funcion );
    virtual ~GeneticAlgorithm();
    R Iteration( int num , int a , int b);

private:

    /* utility function */
    int Selected( Population& input );
    void CrossoverMutation( Population& input , int b );
    void NewPopulation( Population& input, int a , int b );
    bool Evaluation( Population& input , R &prev );

    /* Parameters */
    F* Function;
};

////////////// body of function

template< class Tr >
GeneticAlgorithm< Tr >::GeneticAlgorithm(){
    Function = 0;
}

template< class Tr >
GeneticAlgorithm< Tr >::~GeneticAlgorithm(){
    Function = 0;
}

template< class Tr >
void GeneticAlgorithm< Tr >::GetFunction(F funcion){
    this->Function = &funcion;
    return;
}

template<  class Tr >
typename GeneticAlgorithm< Tr >::R GeneticAlgorithm< Tr >::Iteration( int num , int a , int b ){
    vector< R > population;
    srand( time( 0 ) );

    for( ; num > 0 ; num--  ){  population.push_back( rand()%( b - a + 1 ) + a ); }
    R previos;
    int clausula = 0;

    do{
        clausula++;
        cout << endl<< "====> Iteracion: " << clausula << endl;
        Selected( population );
        CrossoverMutation( population, b );
        usleep( 100000 );
    }while( clausula < 10  && Evaluation( population, previos) );

    cout << endl << " Rpta: "<< previos;
    return previos;

}

/* function of utility */

template< class Tr >/// warning
bool GeneticAlgorithm< Tr >::Evaluation( Population & input  , R& rpta){
    map< R , int > Evaluation;

    for( int i = 0; i < input.size()  ; i++ ){
        if( Evaluation.find( input[ i ] ) == Evaluation.end() ){
            Evaluation[ input[ i ] ] = 0;
        }
        Evaluation[ input[ i ] ]++;
    }

    for( map< R , int >::iterator itr = Evaluation.begin() ; itr != Evaluation.end() ; itr++ ){
        if( ( float ) (*itr).second / input.size() >= 0.5  ){
            rpta = (*itr).first;
            return false;
        }
    }

    return true;
}

/* Selected based by Roulette*/
template< class Tr >
int GeneticAlgorithm< Tr >::Selected( Population &input ){
    vector< pair< float, R > > Ranking;
    int Total = 0;
    cout << "######################################" << endl;
    cout << " Poblacion actual: " << endl;
    for( int i = 0 ; i < ( int ) input.size() ; i++ ){
        cout << input[ i ] << "-" ;
        Total += Function->operator()( input[ i ] );
        Ranking.push_back( make_pair< float , R >( 0.0, input[ i ] ) );
    }
    cout << endl;
    double medio = ( double)Total/ input.size();

    cout << endl << "la media de los valores: " << medio << endl;
    /// function de espera
    for( int i = 0 ; i < ( int ) input.size() ; i++ ){
        Ranking[ i ].first  = ( ( float ) ( Function->operator()( input[ i ] ) ) / medio );
        cout << "Valor de Seleccion: "<< Ranking[ i ].first << " ,Item: " << Ranking[ i ].second <<" ,Valor de la funcion: " <<  Function->operator()( input[ i ] )  << endl;
    }

    cout << "-------------"<<endl;
    /// Selection
    sort( Ranking.begin() , Ranking.end() );
    reverse( Ranking.begin() , Ranking.end() );
    input.clear();

    for( int i = 0 ; Ranking.size() > i  ; i++ ){
        int limite = round( Ranking[ i ].first );
        if( limite > 0 ){
            while( limite-- ){
                input.push_back( Ranking[i].second );
            }
        }
    }
    /// promedio
    return Total;
}


template< class Tr >
void GeneticAlgorithm< Tr >::CrossoverMutation(Population &input , int b){
    srand( time( 0 ) );
    int index = 0;
    int swap1 = 0;
    int swap2 = 0;
    int X = 0;
    int Y = 0;
    int i = 0;

    for( int  j = input.size() ; j > 1 ; j -= 2 ){
        index = rand()%( 8 ) + 1;
        X = rand()%( input.size() - 1 );
        Y = rand()%( input.size() - 1 );

        swap1 = ( input[ X ] & ( ( 1 << index ) - 1 ) ) ;
        swap2 = input[ Y ] & ( ( 1 << index ) - 1 );
        input[ X ] = ( input[ X ] ^ swap1 ) | swap2;
        while( input[ X ] > b ){ input[ X ] -= b; }
        input[ Y ] = ( input[ Y  ] ^ swap2 ) | swap1;
        while( input[ Y ] > b ){ input[ Y ] -= b; }
        i += 2;
    }
    cout << "Nueva Poblacion: " << endl;
    for( int i = 0; i < input.size() ; i++ ){
        cout << input[ i ] << " ";
    }

    return ;
}

#include <bits/stdc++.h>
#include <stdlib.h>
#include <time.h>
using namespace std;
#define INF 2000
#define MAXN 200
#define DIR 4 
struct edge{int to, length;};
struct position{ int x, y;};
typedef vector< vector< edge > > Adjacency;
typedef pair<int,int> pii;
int dx[] = {1,0,-1,0};
int dy[] = {0,1,0,-1};
int Dist[MAXN][MAXN];// VECTOR DE DISTANCIAS.
Adjacency List;
vector<int> who;
char Matrix[MAXN][MAXN];
int n;

void initMatrix()
{
  srand(time(NULL));
  for(int i = 0 ; i < MAXN; ++i)
    for(int j = 0 ; j < MAXN ; ++j)
      {
	Matrix[i][j] = char( (rand() % 1 + 1) + 48 );
      }
  Matrix[0][0] = '0';
}

void inputMatrix()
{
  cin >> n;
  for(int i = 0 ; i < n ; ++i)
    {
      for(int j = 0 ; j < n ; ++j)
	{
	  cin >> Matrix[i][j];
	}
    }
}


void Display()
{
  //initMatrix();
  inputMatrix();
  for(int i = 0 ; i < n ; ++i)
    {
      for(int j = 0 ; j <n  ; ++j)
	{
	  cout << Matrix[i][j] << " "; 
	}
      cout << "\n";
    }
}

void initDistancia()
{
  for(int i = 0 ; i < MAXN ; ++i)
    {
      for(int j =0 ; j < MAXN; ++j)
	{
	  Dist[i][j] = INF;
	}
    }
}

int Heuristic(pii source, pii goal)// Manhattan Distance
{
  return abs(goal.first - 1  - source.first) + abs(goal.second - 1 - source.second);
}

int Nothing(pii source, pii goal)
{
  return 0;
}

void displayMatrix(int cnt)
{
  cout << "\n\n";
  cout << "------------------------------------\n";
  cout << "------------------------------------\n";
  char a = '*';
  for(int i = 0 ; i < n ; ++i)
    {
      for(int j = 0 ; j < n; ++j)
	{
	  if(Dist[i][j] == 2000)
	    printf("%4c",a);
	  else
	    printf("%4d",Dist[i][j]);
	}
      cout << "\n";
    }
  cout << "Nodos explorados : " << cnt << "\n";
  cout << "\n\n";
}

// Dijkstra con Colas de Prioridad O( (n + m) * logn )
void aStar(pii source, pii goal, int (*fnc)(pii,pii))
{
  int cnt=0;
  initDistancia();
  Dist[source.first][source.second] = 0; // Init de distancias.
  set< pair <int, pii > > now; // distancia , indice
  now.insert( {0,source} ); // Cola de Prioridad que elige el vertice siguiente a escoger.
  while(!(now.empty()))
    {
      ++cnt;
      pii where = now.begin()->second; // Escoge el nodo mas conveniente.
      int dist = now.begin()->first;
      now.erase(now.begin());
      if(where.first == goal.first - 1 and where.second == goal.first-1)
	break;
      // cout << where.first << " " << where.second <<  " --- "  << dist  << " " << now.size()<< "\n";
      for(int i = 0 ; i < DIR; ++i)// Recorremos 
  	{
	  int movx = where.first + dx[i], movy =  where.second + dy[i];
	  if(movx >= 0 and movx < n and movy >= 0 and movy < n) 
	    {
	      if( Dist[movx][movy] >
		  Dist[where.first][where.second] + int(Matrix[movx][movy]-48)) // si existe una arista mas conveniente go.
		{
		  now.erase( {Dist[movx][movy] + fnc(goal,{movx,movy}) , {movx,movy} } ); // borramos el que existia.
		  Dist[movx][movy] = Dist[where.first][where.second] + int(Matrix[movx][movy]-48); //actualizamos vector de dist.
		  now.insert( {Dist[movx][movy] + fnc(goal,where) , {movx,movy} } ); // actualizamos priority queue
		}
	    }
  	}
    }

  displayMatrix(cnt);
}


void initGrafo()
{
  cout << "Con Dijkstra : \n";
  aStar({0,0},{n,n},&Nothing);
  cout << "Con A * \n" ;
  aStar({0,0},{n,n},&Heuristic);
}

int main()
{
  Display();
  initGrafo();
  return 0;
}

